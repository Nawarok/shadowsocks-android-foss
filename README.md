## [Shadowsocks](https://shadowsocks.org) FOSS for Android

[![API](https://img.shields.io/badge/API-23%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=23)
[![License: GPL-3.0](https://img.shields.io/badge/license-GPL--3.0-orange.svg)](https://www.gnu.org/licenses/gpl-3.0)

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/en/packages/com.gitlab.mahc9kez.shadowsocks.foss/)

This project is a fork of [shadowsocks-andorid](https://github.com/shadowsocks/shadowsocks-android). It aimed to be FOSS, ad-free, does not require any proprietary dependencies or services, and does not collect your data nor share with any third party services.

Currently keep updating with upstream, no extra features are added but modified a few default options with different theme.

### PREREQUISITES

* JDK 1.8
* Android SDK
  - Android NDK
* Rust with Android targets installed  
  `$ rustup target install armv7-linux-androideabi aarch64-linux-android i686-linux-android x86_64-linux-android`

### BUILD

You can check whether the latest commit builds under UNIX environment by checking Travis status.

* Install prerequisites
* Clone the repo using `git clone --recurse-submodules <repo>` or update submodules using `git submodule update --init --recursive`
* `sed -i 's/com.github.shadowsocks/com.gitlab.mahc9kez.shadowsocks.foss/' core/src/main/jni/badvpn/tun2socks/tun2socks.c`
* Build it using Android Studio or gradle script

### BUILD WITH DOCKER

* Clone the repo using `git clone --recurse-submodules <repo>` or update submodules using `git submodule update --init --recursive`
* Run `docker run --rm -v ${PWD}:/build -w /build shadowsocks/android-ndk-go ./gradlew assembleDebug`

### VERSIONING

Version Name:
Same as upstream releases but [a-z] suffix for each revision

```
 [4]  [08]  [07]  [01]  0
 4     .8    .7     a   [ABI]

ABI:
0 universal
1 armeabi-v7a
2 arm64-v8a
3 x86
4 x86_64
```

### CONTRIBUTING

If you are interested in contributing or getting involved with this project, please read the CONTRIBUTING page for more information.  The page can be found [here](https://gitlab.com/mahc9kez/shadowsocks-android-foss/-/blob/master/CONTRIBUTING.md).

### TRANSLATION

We keep most of translation from [upstream](https://discourse.shadowsocks.org/t/poeditor-translation-main-thread/30), except Simplified Chinese (zh-rCN) translation. This project does not include or accept zh-rCN translation.

F-Droid translation are located in [metadata/](metadata/), you can contribute directly by sending Merge Requests.

## OPEN SOURCE LICENSES

<ul>
    <li>redsocks: <a href="https://github.com/shadowsocks/redsocks/blob/shadowsocks-android/README">APL 2.0</a></li>
    <li>libevent: <a href="https://github.com/shadowsocks/libevent/blob/master/LICENSE">BSD</a></li>
    <li>tun2socks: <a href="https://github.com/shadowsocks/badvpn/blob/shadowsocks-android/COPYING">BSD</a></li>
    <li>shadowsocks-rust: <a href="https://github.com/shadowsocks/shadowsocks-rust/blob/master/LICENSE">MIT</a></li>
    <li>libsodium: <a href="https://github.com/jedisct1/libsodium/blob/master/LICENSE">ISC</a></li>
    <li>OpenSSL: <a href="https://www.openssl.org/source/license-openssl-ssleay.txt">OpenSSL License</a></li>
</ul>


### LICENSE

Files under [art/](art/) folder are used to generate certain icons which are licensed under Apache-2.0

Based heavily on [shadowsocks-andorid](https://github.com/shadowsocks/shadowsocks-android) by Max Lv and Mygod Studio

Copyright (C) 2017 by Max Lv <<max.c.lv@gmail.com>>  
Copyright (C) 2017 by Mygod Studio <<contact-shadowsocks-android@mygod.be>>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
