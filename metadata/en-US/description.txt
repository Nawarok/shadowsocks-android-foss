Shadowsocks FOSS is an ad-free FOSS client for Shadowsocks, it does not require any proprietary dependencies or services.

Shadowsocks is a high-performance cross-platform secured SOCKS5 proxy. It will help you surf the internet privately and securely.

### FEATURES ###

1. Bleeding edge techniques with Asynchronous I/O and Event-driven programming.
2. Low resource comsumption, suitable for low end boxes and embedded devices.
3. Avaliable on multiple platforms, including PC, MAC, Mobile (Android and iOS) and Routers (OpenWRT).
4. Open source implementions in python, node.js, golang, C#, and pure C.

More details about Shadowsocks: https://shadowsocks.org

### SETUP ###

The App itself does not collect your data, but your proxy provider may do.
you may need to self-host your own Shadowsocks server, details: https://shadowsocks.org/en/download/servers.html

### PLUGINS ###

V2Ray Plugin
https://f-droid.org/en/packages/com.github.shadowsocks.plugin.v2ray/
