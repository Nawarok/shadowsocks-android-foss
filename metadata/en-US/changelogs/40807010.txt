* Remove the build-in QR Code scanner
* Support Android 6.0+
* Default Route to Bypass LAN
* Use Cloudflare DNS as default Remote DNS
* Enable Send DNS over UDP by default
* Enable IPv6 Route by default
