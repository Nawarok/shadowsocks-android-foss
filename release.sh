#!/bin/bash

release=$1
cp mobile/build/outputs/apk/armv7/release/mobile-armv7-release.apk shadowsocks-foss-armeabi-v7a-${release}.apk
cp mobile/build/outputs/apk/arm64/release/mobile-arm64-release.apk shadowsocks-foss-arm64-v8a-${release}.apk
cp mobile/build/outputs/apk/x86/release/mobile-x86-release.apk shadowsocks-foss-x86-${release}.apk
cp mobile/build/outputs/apk/x64/release/mobile-x64-release.apk shadowsocks-foss-x86_64-${release}.apk
cp mobile/build/outputs/apk/afat/release/mobile-afat-release.apk shadowsocks-foss--universal-${release}.apk
cp tv/build/outputs/apk/armv7/release/tv-armv7-release.apk shadowsocks-tv-foss-armeabi-v7a-${release}.apk
cp tv/build/outputs/apk/arm64/release/tv-arm64-release.apk shadowsocks-tv-foss-arm64-v8a-${release}.apk
cp tv/build/outputs/apk/x86/release/tv-x86-release.apk shadowsocks-tv-foss-x86-${release}.apk
cp tv/build/outputs/apk/x64/release/tv-x64-release.apk shadowsocks-tv-foss-x86_64-${release}.apk
cp tv/build/outputs/apk/afat/release/tv-afat-release.apk shadowsocks-tv-foss--universal-${release}.apk
